# Job defintions in this repository:

1. For running a ci build including both java and nodejs builds followed by a test execution, use the content of .gitlab-ci-regular in .gitlab-ci

2. For creating a new release, for example having version 1.0.0, use the .gitlab-release in .gitlab-ci run the following command:

git tag v1.0.0
git push origin v1.0.0

3. For testing a release, use .gitlab-test-release in .gitlab-ci, and set the RELEASE_TAG environment variable to point to the release to be tested:

 RELEASE_TAG: "1.0.0"
 
 and make sure the environment variables are valid for execution and trigger the job manually from gitlab.

 
 
