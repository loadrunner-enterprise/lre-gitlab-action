import { exec } from 'child_process';
import * as fs from 'fs/promises';
import * as path from 'path';
import { spawn } from 'child_process';

// Validate must environment variables only
const validateEnvVars = () => {
  const requiredEnvVars = [
    'lre_server', 'lre_domain', 
	'lre_username', 'lre_password',
    'lre_project', 'lre_test'
  ];

  requiredEnvVars.forEach(envVar => {
    if (!process.env[envVar]) {
      throw new Error(`required ${envVar} environment variable is not set.`);
    }
  });
};

// Validate environment variables before proceeding
validateEnvVars();

// get all the enviroment variables into variables for further validation or replacement with default value
let lreAction = process.env.lre_action;
let lreDescription = process.env.lre_description;
let lreServer = process.env.lre_server;
let lreHttpsProtocol: boolean = process.env.lre_https_protocol === 'true' ? true : false;
let lreAuthenticateWithToken: boolean = process.env.lre_authenticate_with_token === 'true' ? true : false;
let lreUsername = process.env.lre_username;
let lrePassword = process.env.lre_password;
let lreDomain = process.env.lre_domain;
let lreProject = process.env.lre_project;
let lreTest = process.env.lre_test;
let lreTestInstance = process.env.lre_test_instance;
let lreTimeslotDurationHours: number = process.env.lre_timeslot_duration_hours && parseInt(process.env.lre_timeslot_duration_hours) > 0 ? parseInt(process.env.lre_timeslot_duration_hours) : 0;
let lreTimeslotDurationMinutes: number = process.env.lre_timeslot_duration_minutes && parseInt(process.env.lre_timeslot_duration_minutes) > 0 ? parseInt(process.env.lre_timeslot_duration_minutes) : 0;
let lrePostRunAction = process.env.lre_post_run_action;
let lreVudsMode: boolean = process.env.lre_vuds_mode === 'true' ? true : false;
let lreTrendReport = process.env.lre_trend_report;
let lreProxyOutUrl = process.env.lre_proxy_out_url;
let lreUsernameProxy = process.env.lre_username_proxy;
let lrePasswordProxy = process.env.lre_password_proxy;
let lreSearchTimeslot: boolean = process.env.lre_search_timeslot === 'true' ? true : false;
let lreStatusBySla: boolean = process.env.lre_status_by_sla === 'true' ? true : false;
let lreOutputDir = process.env.lre_output_dir;
let lreEnableStacktrace: boolean = process.env.lre_enable_stacktrace === 'true' ? true : false;
const workspaceDir = process.env.CI_PROJECT_DIR;

const validateInputVars = () => {
// Validate 'lre_action' parameter
	  if (!lreAction) {
		  lreAction = 'ExecuteLreTest';
	  }
	  
	  // Validate 'lre_description' parameter
	  if (!lreDescription) {
		  lreDescription = 'Executing LRE test';
	  }
	  
	  // Validate 'lre_server' parameter
	  if (!lreServer) {
		  throw new Error(`lre_server environment variable is not set.`);
	  }

	  // Validate 'lre_https_protocol' parameter
	  if (lreHttpsProtocol !== true && lreHttpsProtocol !== false) {
		  lreHttpsProtocol = false;
      }
	  
	  // Validate 'lre_authenticate_with_token' parameter
	  if (lreAuthenticateWithToken !== true && lreAuthenticateWithToken !== false) {
		  lreAuthenticateWithToken = false;
	  }

	  // Validate 'lre_username' parameter
	  if (!lreUsername) {
		  throw new Error(`lre_username environment variable is not set.`);
	  }
	  
	  // Validate 'lre_password' parameter
	  if (!lrePassword) {
		  throw new Error(`lre_password environment variable is not set.`);
	  }
	  
	  // Validate 'lre_domain' parameter
	  if (!lreDomain) {
		  throw new Error(`lre_domain environment variable is not set.`);
	  }
	  
	  // Validate 'lre_project' parameter
	  if (!lreProject) {
		  throw new Error(`lre_project environment variable is not set.`);
	  }
	  
	  // Validate 'lre_test' parameter
	  if (!lreTest) {
		  throw new Error(`lre_test environment variable is not set.`);
	  }
	  
	  // Validate 'lre_test_instance' parameter
	  if (!lreTestInstance || parseInt(lreTestInstance) <= 0) {
		  lreTestInstance = 'AUTO';
	  }
	  
	  // Validate 'lre_timeslot_duration_hours' parameter
	  if (!lreTimeslotDurationHours) {
		  lreTimeslotDurationHours = 0;
	  }
	  
	  // Validate 'lre_timeslot_duration_minutes' parameter
	  if (!lreTimeslotDurationMinutes || lreTimeslotDurationMinutes < 30) {
		  if(lreTimeslotDurationHours < 1) {
			  lreTimeslotDurationMinutes = 30;
		  }
	  }
	  
	  // Validate 'lre_post_run_action' parameter
	  if (!lrePostRunAction) {
		  lrePostRunAction = 'Do Not Collate';
	  }
	  
	  // Validate 'lre_vuds_mode' parameter
	  if (lreVudsMode !== true && lreVudsMode !== false) {
		  lreVudsMode = false;
	  }
	  
	  // Validate 'lre_trend_report' parameter
	  if (!lreTrendReport) {
		  lreTrendReport = '';
	  }
	  
	  // Validate 'lre_proxy_out_url' parameter
	  if (!lreProxyOutUrl) {
		  lreProxyOutUrl = '';
	  }
	  
	  // Validate 'lre_username_proxy' parameter
	  if (!lreUsernameProxy) {
		  lreUsernameProxy ='';
	  }
	  
	  // Validate 'lre_password_proxy' parameter
	  if (!lrePasswordProxy) {
		  lrePasswordProxy = '';
	  }
	  
	  // Validate 'lre_search_timeslot' parameter
	  if (lreSearchTimeslot !== true && lreSearchTimeslot !== false) {
		  lreSearchTimeslot = false;
	  }
	  
	  // Validate 'lre_status_by_sla' parameter
	  if (lreStatusBySla !== true && lreStatusBySla !== false) {
		  lreStatusBySla = false;
	  }
	  
	  // Validate 'lre_output_dir' parameter
	  if (!lreOutputDir) {
		  lreOutputDir = workspaceDir;
	  }
	  
	  // Validate 'lre_enable_stacktrace' parameter
	  if (lreEnableStacktrace !== true && lreEnableStacktrace !== false) {
		  lreEnableStacktrace = false;
	  }
};


// Define configuration object
const config = {
  lre_action: lreAction,
  lre_description: lreDescription,
  lre_server: lreServer,
  lre_https_protocol: lreHttpsProtocol,
  lre_authenticate_with_token: lreAuthenticateWithToken,
  lre_domain: lreDomain,
  lre_project: lreProject,
  lre_test: lreTest,
  lre_test_instance: lreTestInstance,
  lre_timeslot_duration_hours: lreTimeslotDurationHours,
  lre_timeslot_duration_minutes: lreTimeslotDurationMinutes,
  lre_post_run_action: lrePostRunAction,
  lre_vuds_mode: lreVudsMode,
  lre_trend_report: lreTrendReport,
  lre_proxy_out_url: lreProxyOutUrl,
  lre_search_timeslot: lreSearchTimeslot,
  lre_status_by_sla: lreStatusBySla,
  lre_output_dir: lreOutputDir,
  lre_enable_stacktrace: lreEnableStacktrace
};

const writeConfigFile = async () => {
  const configFilePath = path.join(process.cwd(), 'config.json');
  await fs.writeFile(configFilePath, JSON.stringify(config, null, 2));
  return configFilePath;
};

// const triggerMavenApp = (configFilePath: string) => {
  // exec(`mvn -f ../java-app/pom.xml exec:java -Dexec.args="${configFilePath}"`, (error, stdout, stderr) => {
    // if (error) {
      // console.error(`exec error: ${error}`);
      // return;
    // }
    // console.log(`stdout: ${stdout}`);
    // console.error(`stderr: ${stderr}`);
  // });
// };

const triggerJavaJarApp = (configFilePath: string) => {
	// Path to the JAR file
	  const jarFilePath = path.resolve(__dirname, 'lre-actions-1.1-SNAPSHOT-jar-with-dependencies.jar');
	  
	  // Java application command and parameters
	  const javaAppCommand = 'java';
	  const javaAppArgs = ['-cp', jarFilePath, 'com.opentext.lre.actions.Main', configFilePath]; // Add any additional arguments here
	  
	  // Set the working directory explicitly
	  const workingDirectory = __dirname; // Or specify the desired working directory
	  
	  // Spawn the Java process
	  const javaProcess = spawn(javaAppCommand, javaAppArgs, { cwd: workingDirectory });
	  javaProcess.on('error', (err: any) => {
		  console.error('Failed to start Java process:', err);
	  });
	   // Handle data from Java process
	  javaProcess.stdout.on('data', (data: any) => {
		  console.log(`${data}`);
	  });
	  
	  // Handle errors from Java process
	  javaProcess.stderr.on('data', (data: any) => {
		  console.error(`${data}`);
	  });
	  
	  // Handle Java process exit
	  javaProcess.on('close', (code: any) => {
		  if (code !== 0) {
			  throw new Error(`process exited with code ${code}`);
		  } else {
			  console.log('process completed successfully.');
		  }
	  });
}

const main = async () => {
  try {
    const configFilePath = await writeConfigFile();
	triggerJavaJarApp(configFilePath);
  } catch (error) {
    console.error("Error:", error);
    process.exit(1);
  }
};

main();
