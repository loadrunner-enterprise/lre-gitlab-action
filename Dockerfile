# Use an official Maven image as a base
FROM maven:3.8.4-jdk-11

# Install Node.js
RUN apt-get update && \
    apt-get install -y curl && \
    curl -fsSL https://deb.nodesource.com/setup_20.x | bash - && \
    apt-get install -y nodejs && \
    apt-get clean

# Verify installations
RUN mvn -v && node -v && npm -v

# Set the working directory
WORKDIR /app
